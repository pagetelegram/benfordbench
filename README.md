# Benfordbench

Benford Bench is software developed for fraud detection using Benford's Law. More details about our project at https://benfordbench.org

bxc.bas // offers command line automation with flag support:
bxc -f [data file] -d [all] or [1] -l [length of sample pools (ie 10000)] -c [column number] - {terminal}